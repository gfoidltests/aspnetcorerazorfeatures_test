﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers
{
    public class Email1TagHelper : TagHelper
    {
        private const string EmailDomain = "contoso.com";
        //---------------------------------------------------------------------
        public string MailTo { get; set; }
        //---------------------------------------------------------------------
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (this.MailTo == null)
            {
                this.MailTo = context
                    .AllAttributes
                    .FirstOrDefault(a => a.Name == "mail-to")
                    ?.Value.ToString();
            }

            string address = $"{this.MailTo}@{EmailDomain}";

            output.TagName = "a";
            output.Attributes.SetAttribute("href", $"mailto:{address}");
            output.Content.SetContent(address);
        }
    }
    //-------------------------------------------------------------------------
    public class Email2TagHelper : TagHelper
    {
        private const string EmailDomain = "contoso.com";
        //---------------------------------------------------------------------
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            TagHelperContent content = await output.GetChildContentAsync();
            string address = $"{content.GetContent()}@{EmailDomain}";

            output.TagName = "a";
            output.Attributes.SetAttribute("href", $"mailto:{address}");
            output.Content.SetContent(address);
        }
    }
    //-------------------------------------------------------------------------
    public class EmailTagHelper : TagHelper
    {
        private const string EmailDomain = "contoso.com";
        //---------------------------------------------------------------------
        public string MailTo { get; set; }
        //---------------------------------------------------------------------
        public override void Process(TagHelperContext context, TagHelperOutput output) => base.Process(context, output);

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            string mailTo = context
                .AllAttributes
                .FirstOrDefault(a => a.Name == "mail-to")
                ?.Value.ToString();

            if (string.IsNullOrWhiteSpace(mailTo))
            {
                TagHelperContent content = await output.GetChildContentAsync();
                mailTo                   = content.GetContent();
            }

            string address = $"{mailTo}@{EmailDomain}";

            output.TagName = "a";
            output.Attributes.SetAttribute("href", $"mailto:{address}");
            output.Content.SetContent(address);
        }
    }
}
