﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace TagHelpers
{
    public class ConditionTagHelper : TagHelper
    {
        public bool Condition { get; set; }
        //---------------------------------------------------------------------
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (!this.Condition)
                output.SuppressOutput();
        }
    }
}
