﻿using System;
using Microsoft.AspNetCore.Razor.TagHelpers;
using TagHelpers.Models;

namespace TagHelpers
{
    [HtmlTargetElement("website-information", TagStructure = TagStructure.WithoutEndTag)]
    public class WebsiteInformationTagHelper : TagHelper
    {
        public WebsiteContext Info { get; set; }
        //---------------------------------------------------------------------
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            this.EnsureInfo();

            output.TagName = "section";
            output.Content.SetHtmlContent(
$@"
    <ul>
        <li><strong>Version:</strong> {this.Info.Version}</li>
        <li><strong>Copyright Year:</strong> {this.Info.CopyrightYear}</li>
        <li><strong>Approved:</strong> {this.Info.Approved}</li>
        <li><strong>Number of tags to show:</strong> {this.Info.TagsToShow}</li>
    </ul>
");
            output.TagMode = TagMode.StartTagAndEndTag;
        }
        //---------------------------------------------------------------------
        private void EnsureInfo()
        {
            if (this.Info != null) return;

            this.Info = new WebsiteContext
            {
                Approved = true,
                CopyrightYear = DateTime.Now.Year,
                TagsToShow = 242,
                Version = new Version(2, 1, 135, 5)
            };
        }
    }
}
