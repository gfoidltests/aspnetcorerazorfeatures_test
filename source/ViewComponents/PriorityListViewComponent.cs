﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ViewComponents.Models;

namespace ViewComponents
{
    public class PriorityListViewComponent : ViewComponent
    {
        private List<TodoItem> _items;
        //---------------------------------------------------------------------
        public PriorityListViewComponent()
        {
            _items = new List<TodoItem>();

            for (int i = 0; i < 9; i++)
            {
                _items.Add(new TodoItem()
                {
                    IsDone = i % 3 == 0,
                    Name = "Task " + (i + 1),
                    Priority = i % 5 + 1
                });
            }
        }
        //---------------------------------------------------------------------
        public IViewComponentResult Invoke(int maxPriority, bool isDone)
        {
            List<TodoItem> items = this.GetItems(maxPriority, isDone);

            return this.View(items);
        }
        //---------------------------------------------------------------------
        private List<TodoItem> GetItems(int maxPriority, bool isDone)
        {
            return _items
                .Where(x => x.IsDone == isDone && x.Priority <= maxPriority)
                .ToList();
        }
    }
}
