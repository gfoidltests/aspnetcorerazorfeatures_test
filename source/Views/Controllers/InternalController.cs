﻿using Microsoft.AspNetCore.Mvc;

namespace Views.Controllers
{
    public class InternalController : Controller
    {
        public IActionResult Index()
        {
            this.ViewData["Message"] = "Your internal description page.";

            return this.View();
        }
        //---------------------------------------------------------------------
        public IActionResult About()
        {
            this.ViewData["Message"] = "Your application description page.";

            return this.View();
        }
    }
}
