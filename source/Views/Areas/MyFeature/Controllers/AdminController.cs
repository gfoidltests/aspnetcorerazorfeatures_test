﻿using Microsoft.AspNetCore.Mvc;

namespace Views.Areas.MyFeature.Controllers
{
    [Area("MyFeature")]
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            this.ViewData["Message"] = "Foo";
            return this.View();
        }
    }
}
