﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using NUnit.Framework;

namespace TagHelpers.Tests
{
    [TestFixture]
    public class Email1TagHelperTests
    {
        [Test]
        public void Email_as_attribute___substituted()
        {
            var tagHelperContext = new TagHelperContext(
                "email1",
                new TagHelperAttributeList(new[] { new TagHelperAttribute("mail-to", "foo") }),
                new Dictionary<object, object>(),
                Guid.NewGuid().ToString("N"));

            var tagHelperOutput = new TagHelperOutput(
                "email1",
                new TagHelperAttributeList(),
                (result, encoder) =>
                {
                    TagHelperContent tagHelperContent = new DefaultTagHelperContent();
                    return Task.FromResult(tagHelperContent);
                });

            var sut = new Email1TagHelper();
            sut.Process(tagHelperContext, tagHelperOutput);

            var writer = new StringWriter();
            tagHelperOutput.WriteTo(writer, NullHtmlEncoder.Default);
            string actual = writer.ToString();

            string expected = "<a href=\"mailto:foo@contoso.com\">foo@contoso.com</a>";

            Assert.AreEqual(expected, actual);
        }
    }
}
