﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using NUnit.Framework;
using Views.TagHelpers;

namespace Views.Tests.TagHelpers
{
    [TestFixture]
    public class EmailTagHelperTests
    {
        [Test]
        public async Task Email_as_attribute___substituted()
        {
            var tagHelperContext = new TagHelperContext(
                "email",
                new TagHelperAttributeList(new[] { new TagHelperAttribute("mail-to", "foo") }),
                new Dictionary<object, object>(),
                Guid.NewGuid().ToString("N"));

            var tagHelperOutput = new TagHelperOutput(
                "email",
                new TagHelperAttributeList(),
                (result, encoder) =>
                {
                    TagHelperContent tagHelperContent = new DefaultTagHelperContent();
                    return Task.FromResult(tagHelperContent);
                });

            var sut = new EmailTagHelper();
            await sut.ProcessAsync(tagHelperContext, tagHelperOutput);

            var writer = new StringWriter();
            tagHelperOutput.WriteTo(writer, NullHtmlEncoder.Default);
            string actual = writer.ToString();

            string expected = "<a href=\"mailto:foo@contoso.com\">foo@contoso.com</a>";

            Assert.AreEqual(expected, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Email_as_content___substituted()
        {
            var tagHelperContext = new TagHelperContext(
                //"email",      // here optional
                new TagHelperAttributeList(),
                new Dictionary<object, object>(),
                Guid.NewGuid().ToString("N"));

            var tagHelperOutput = new TagHelperOutput(
                "email",
                new TagHelperAttributeList(),
                (result, encoder) =>
                {
                    TagHelperContent tagHelperContent = new DefaultTagHelperContent();
                    tagHelperContent.SetContent("foo");
                    return Task.FromResult(tagHelperContent);
                });

            var sut = new EmailTagHelper();
            await sut.ProcessAsync(tagHelperContext, tagHelperOutput);

            var writer = new StringWriter();
            tagHelperOutput.WriteTo(writer, NullHtmlEncoder.Default);
            string actual = writer.ToString();

            string expected = "<a href=\"mailto:foo@contoso.com\">foo@contoso.com</a>";

            Assert.AreEqual(expected, actual);
        }
        //---------------------------------------------------------------------
        [Test]
        public async Task Email_as_attribute_and_content___attribute_is_used()
        {
            var tagHelperContext = new TagHelperContext(
                "email",
                new TagHelperAttributeList(new[] { new TagHelperAttribute("mail-to", "foo") }),
                new Dictionary<object, object>(),
                Guid.NewGuid().ToString("N"));

            var tagHelperOutput = new TagHelperOutput(
                "email",
                new TagHelperAttributeList(),
                (result, encoder) =>
                {
                    TagHelperContent tagHelperContent = new DefaultTagHelperContent();
                    tagHelperContent.SetContent("bar");
                    return Task.FromResult(tagHelperContent);
                });

            var sut = new EmailTagHelper();
            await sut.ProcessAsync(tagHelperContext, tagHelperOutput);

            var writer = new StringWriter();
            tagHelperOutput.WriteTo(writer, NullHtmlEncoder.Default);
            string actual = writer.ToString();

            string expected = "<a href=\"mailto:foo@contoso.com\">foo@contoso.com</a>";

            Assert.AreEqual(expected, actual);
        }
    }
}
