﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using NUnit.Framework;
using Views.TagHelpers;

namespace Views.Tests.TagHelpers
{
    [TestFixture]
    public class Email2TagHelperTests
    {
        [Test]
        public async Task Email_as_content___substituted()
        {
            var tagHelperContext = new TagHelperContext(
                //"email2",     // here optional
                new TagHelperAttributeList(),
                new Dictionary<object, object>(),
                Guid.NewGuid().ToString("N"));

            var tagHelperOutput = new TagHelperOutput(
                "email2",
                new TagHelperAttributeList(),
                (result, encoder) =>
                {
                    TagHelperContent tagHelperContent = new DefaultTagHelperContent();
                    tagHelperContent.SetContent("foo");
                    return Task.FromResult(tagHelperContent);
                });

            var sut = new Email2TagHelper();
            await sut.ProcessAsync(tagHelperContext, tagHelperOutput);

            var writer = new StringWriter();
            tagHelperOutput.WriteTo(writer, NullHtmlEncoder.Default);
            string actual = writer.ToString();

            string expected = "<a href=\"mailto:foo@contoso.com\">foo@contoso.com</a>";

            Assert.AreEqual(expected, actual);
        }
    }
}
